package com.dbsoftwares.core.api.user;

/*
 * Created by DBSoftwares on 21/02/2018
 * Developer: Dieter Blancke
 * Project: DBSCore
 */

public interface User {

    boolean isPlayer();

    boolean isProxiedPlayer();

    Object getPlayer();

    Object getProxiedPlayer();

    String getName();

    boolean hasPermission(String permission);
}