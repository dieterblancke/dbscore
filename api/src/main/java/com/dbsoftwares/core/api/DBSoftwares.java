package com.dbsoftwares.core.api;

/*
 * Created by DBSoftwares on 19/02/2018
 * Developer: Dieter Blancke
 * Project: DBSCore
 */

public class DBSoftwares {

    private static DBSApi api;

    /**
     * Returns an instance of the {@link DBSApi},
     *
     * @return a DBSApi instance (if initialized)
     * @throws IllegalStateException when the API hasnet been initialized.
     */
    public static DBSApi getApi() throws IllegalStateException {
        if (api == null) { // api not initialized?
            throw new IllegalStateException("API is not initialized.");
        }

        return api;
    }
}