package com.dbsoftwares.core.api.placeholder;

import com.dbsoftwares.core.api.user.User;

public class PlaceHolderEvent {
	
	private User user;
	private PlaceHolder placeholder;
	private String message;
	
	public PlaceHolderEvent(User user, PlaceHolder placeholder, String message){
		this.user = user;
		this.placeholder = placeholder;
		this.message = message;
	}
	
	public String getMessage(){
		return message;
	}
	
	public User getUser(){
		return user;
	}
	
	public PlaceHolder getPlaceHolder(){
		return placeholder;
	}

	public String getArg() {
		String[] words = message.split(" ");

		for(String word : words) {
		    if(word.contains(placeholder.getPlaceHolder())) {
		    	word = word.substring(word.lastIndexOf("%") + 1);
		        return word.isEmpty() ? null : word;
            }
        }

        return null;
    }
}