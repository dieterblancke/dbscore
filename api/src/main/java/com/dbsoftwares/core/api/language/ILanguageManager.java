package com.dbsoftwares.core.api.language;

/*
 * Created by DBSoftwares on 15 oktober 2017
 * Developer: Dieter Blancke
 * Project: BungeeUtilisals
 */

import com.dbsoftwares.core.api.configuration.FileStorageType;
import com.dbsoftwares.core.api.configuration.IConfiguration;

import java.io.File;
import java.util.Optional;

public interface ILanguageManager {

    /**
     * Similar to {@link #registerLanguages(String, Language...)}
     * Registers the languages into memory.
     * @param identifier The plugin identifier.
     * @param languages The languages to be registered.
     */
    void registerLanguages(String identifier, Iterable<Language> languages);

    /**
     * Similar to {@link #registerLanguages(String, Iterable)}
     * Registers the languages into memory.
     * @param identifier The plugin identifier.
     * @param languages The languages to be registered.
     */
    void registerLanguages(String identifier, Language... languages);

    /**
     * @param identifier The plugin identifier.
     * @return The default language, if not found, it will return the first available language, if still not present, null will be returned.
     */
    Language getDefaultLanguage(String identifier);

    /**
     * Method to retrieve a Language by name.
     * @param identifier The plugin identifier for the language.
     * @param language The name of the language.
     * @return An optional containing the language IF present.
     */
    Optional<Language> getLanguage(String identifier, String language);

    /**
     * Registers a identifier into the ILanguageManager
     * @param identifier The identifier you want to register.
     * @param folder The folder in which the languages will be located.
     * @param type The FileStorageType which will be used, Json or Yaml.
     */
    void addPlugin(String identifier, File folder, FileStorageType type);

    /**
     * Loads default language files of a identifier.
     * @param identifier The identifier of which the languages should be loaded.
     */
    void loadLanguages(String identifier);

    /**
     * @param identifier The identifier of which you want to get the File.
     * @param language The language of which you want to get the file.
     * @return The file containing language settings for a certain identifier.
     */
    File getFile(String identifier, Language language);

    /**
     * @param identifier The identifier of which you want to get the JsonConfiguration
     * @param language The language of which you want to get the JsonConfiguration.
     * @return The JsonConfiguration bound to the certain identifier and language.
     */
    IConfiguration getConfig(String identifier, Language language);

    /**
     * @param identifier The identifier you want to check.
     * @param language The language you want to check.
     * @return True if the certain identifier and language is registered, false if not.
     */
    Boolean isRegistered(String identifier, Language language);

    /**
     * @param identifier The identifier of which you want to save the language.
     * @param language The language you want to save.
     * @return True if successful, false if not.
     */
    Boolean saveLanguage(String identifier, Language language);

    /**
     *
     * @param identifier The identifier of which you want to reload the language config.
     * @param language The language config you want to reload.
     * @return True if successful, false if not.
     */
    Boolean reloadConfig(String identifier, Language language);
}