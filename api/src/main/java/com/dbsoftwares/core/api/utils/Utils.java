package com.dbsoftwares.core.api.utils;

/*
 * Created by DBSoftwares on 16/02/2018
 * Developer: Dieter Blancke
 * Project: DBSCore
 */

public class Utils {

    public static String format(String message) {
        return message.replace("&", "§");
    }

    public static boolean isBoolean(Object object) {
        try {
            Boolean.valueOf(object.toString());
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}