package com.dbsoftwares.core.api.placeholder;

public abstract class PlaceHolderEventHandler {
	
	public abstract String getReplace(PlaceHolderEvent event);
	
}
