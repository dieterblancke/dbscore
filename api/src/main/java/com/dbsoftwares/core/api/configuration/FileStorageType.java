package com.dbsoftwares.core.api.configuration;

/*
 * Created by DBSoftwares on 19/02/2018
 * Developer: Dieter Blancke
 * Project: DBSCore
 */

public enum FileStorageType {

    JSON, YAML

}