package com.dbsoftwares.core.api.utils;

import com.google.common.collect.Maps;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

public class ReflectionUtils {

    // Versioning | used to (try and) get NMS versions.
    private static final boolean ISBUKKIT;
    private static final Class<?> BUKKIT;
    private static final Method BUKKIT_GETSERVER;

    private static final boolean ISBUNGEECORD;
    private static final Class<?> BUNGEECORD;
    private static final Method BUNGEECORD_GETINSTANCE;
    private static final Method BUNGEECORD_GETPROTOCOLVERSION;

    static {
        ISBUKKIT = classExists("org.bukkit.Bukkit");
        BUKKIT = ISBUKKIT ? getClass("org.bukkit.Bukkit") : null;
        BUKKIT_GETSERVER = ISBUKKIT ? getMethod(BUKKIT, "getServer") : null;

        ISBUNGEECORD = classExists("net.md_5.bungee.BungeeCord");
        BUNGEECORD = ISBUNGEECORD ? getClass("net.md_5.bungee.BungeeCord") : null;
        BUNGEECORD_GETINSTANCE = ISBUNGEECORD ? getMethod(BUNGEECORD, "getInstance") : null;
        BUNGEECORD_GETPROTOCOLVERSION = ISBUNGEECORD ? getMethod(BUNGEECORD, "getProtocolVersion") : null;
    }

    private static final Map<Class<?>, Class<?>> TYPES = Maps.newHashMap();

    public static ServerVersion getVersion() {
        return ServerVersion.search();
    }

    public static Object getHandle(Class<?> clazz, Object o) {
        try {
            return clazz.getMethod("getHandle").invoke(o);
        } catch (Exception e) {
            return null;
        }
    }

    public static Object getHandle(Object o) {
        try {
            return getMethod("getHandle", o.getClass()).invoke(o);
        } catch (Exception e) {
            return null;
        }
    }

    public static Class<?> getClass(String name) {
        try {
            return Class.forName(name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Class<?> getNMS(String name) {
        String cname = "net.minecraft.server." + getVersionName() + "." + name;
        Class<?> clazz = null;
        try {
            clazz = Class.forName(cname);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clazz;
    }

    public static Class<?> getBukkitClass(String path) {
        String cname = "org.bukkit.craftbukkit." + getVersionName() + "." + path;
        Class<?> clazz = null;
        try {
            clazz = Class.forName(cname);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clazz;
    }

    public static Method getMethod(String name, Class<?> clazz, Class<?>... paramTypes) {
        Class<?>[] t = toPrimitiveTypeArray(paramTypes);
        for (Method m : clazz.getMethods()) {
            Class<?>[] types = toPrimitiveTypeArray(m.getParameterTypes());
            if (m.getName().equals(name) && equalsTypeArray(types, t)) {
                return m;
            }
        }
        return null;
    }

    public static Method getMethod(Class<?> clazz, String name, Class<?>... args) {
        for (Method m : clazz.getMethods()) {
            if (m.getName().equals(name) && (args.length == 0 || classList(args, m.getParameterTypes()))) {
                m.setAccessible(true);
                return m;
            }
        }
        return null;
    }

    public static Method getMethod(String className, PackageType packageType, String methodName, Class<?>... parameterTypes) throws ClassNotFoundException {
        return getMethod(packageType.getClass(className), methodName, parameterTypes);
    }

    public static Field getField(Class<?> clazz, String name) {
        try {
            Field field = clazz.getDeclaredField(name);
            field.setAccessible(true);
            return field;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Field getField(String className, PackageType packageType, String fieldName) throws SecurityException, ClassNotFoundException {
        return getField(packageType.getClass(className), fieldName);
    }

    public static Object getValue(Object instance, Class<?> clazz, String fieldName) throws IllegalArgumentException, IllegalAccessException, SecurityException {
        return getField(clazz, fieldName).get(instance);
    }

    public static Object getValue(Object instance, String className, PackageType packageType, String fieldName) throws IllegalArgumentException, IllegalAccessException, SecurityException, ClassNotFoundException {
        return getValue(instance, packageType.getClass(className), fieldName);
    }

    public static Object getValue(Object instance, String fieldName) throws IllegalArgumentException, IllegalAccessException, SecurityException {
        return getValue(instance, instance.getClass(), fieldName);
    }

    public static void setValue(Object instance, Class<?> clazz, String fieldName, Object value) throws IllegalArgumentException, IllegalAccessException, SecurityException {
        getField(clazz, fieldName).set(instance, value);
    }

    public static void setValue(Object instance, String className, PackageType packageType, String fieldName, Object value) throws IllegalArgumentException, IllegalAccessException, SecurityException, ClassNotFoundException {
        setValue(instance, packageType.getClass(className), fieldName, value);
    }

    public static void setValue(Object instance, String fieldName, Object value) throws IllegalArgumentException, IllegalAccessException, SecurityException {
        setValue(instance, instance.getClass(), fieldName, value);
    }

    public static Constructor<?> getConstructor(Class<?> clazz, Class<?>... parameterTypes) throws NoSuchMethodException {
        return clazz.getConstructor(parameterTypes);
    }

    public static Constructor<?> getConstructor(String className, PackageType packageType, Class<?>... parameterTypes) throws NoSuchMethodException, ClassNotFoundException {
        return getConstructor(packageType.getClass(className), parameterTypes);
    }

    public static String getVersionName() {
        String version = "v1_12_R1";
        if (ISBUKKIT) {
            try {
                version = BUKKIT_GETSERVER.invoke(null).getClass().getPackage().getName().substring(23);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        } else if (ISBUNGEECORD) {
            try {
                Object instance = BUNGEECORD_GETINSTANCE.invoke(null);
                int protocolId = (int) BUNGEECORD_GETPROTOCOLVERSION.invoke(instance);

                version = getVersionFromProtocolId(protocolId);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return version;
    }

    private static String getVersionFromProtocolId(int id) {
        switch (id) {
            default:
            case 340:
            case 338:
            case 335:
                return ServerVersion.MINECRAFT_1_12.getVersion();
            case 316:
                return ServerVersion.MINECRAFT_1_11_1.getVersion();
            case 315:
                return ServerVersion.MINECRAFT_1_11.getVersion();
        }
    }

    public static boolean classExists(String s) {
        try {
            Class.forName(s);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private static Class<?> getPrimitiveType(Class<?> clazz) {
        return TYPES.getOrDefault(clazz, clazz);
    }

    private static Class<?>[] toPrimitiveTypeArray(Class<?>[] classes) {
        int a = classes != null ? classes.length : 0;
        Class<?>[] types = new Class<?>[a];
        for (int i = 0; i < a; i++) {
            types[i] = getPrimitiveType(classes[i]);
        }
        return types;
    }

    private static boolean equalsTypeArray(Class<?>[] a, Class<?>[] o) {
        if (a.length != o.length) {
            return false;
        }
        for (int i = 0; i < a.length; i++) {
            if (!a[i].equals(o[i]) && !a[i].isAssignableFrom(o[i])) {
                return false;
            }
        }
        return true;
    }

    private static boolean classList(Class<?>[] l1, Class<?>[] l2) {
        boolean equal = true;
        if (l1.length != l2.length) {
            return false;
        }
        for (int i = 0; i < l1.length; i++) {
            if (l1[i] != l2[i]) {
                equal = false;
                break;
            }
        }
        return equal;
    }

    public enum PackageType {
        MINECRAFT_SERVER("net.minecraft.server." + getVersionName()),
        CRAFTBUKKIT("org.bukkit.craftbukkit." + getVersionName()),
        CRAFTBUKKIT_BLOCK(CRAFTBUKKIT, "block"),
        CRAFTBUKKIT_CHUNKIO(CRAFTBUKKIT, "chunkio"),
        CRAFTBUKKIT_COMMAND(CRAFTBUKKIT, "command"),
        CRAFTBUKKIT_CONVERSATIONS(CRAFTBUKKIT, "conversations"),
        CRAFTBUKKIT_ENCHANTMENS(CRAFTBUKKIT, "enchantments"),
        CRAFTBUKKIT_ENTITY(CRAFTBUKKIT, "entity"),
        CRAFTBUKKIT_EVENT(CRAFTBUKKIT, "event"),
        CRAFTBUKKIT_GENERATOR(CRAFTBUKKIT, "generator"),
        CRAFTBUKKIT_HELP(CRAFTBUKKIT, "help"),
        CRAFTBUKKIT_INVENTORY(CRAFTBUKKIT, "inventory"),
        CRAFTBUKKIT_MAP(CRAFTBUKKIT, "map"),
        CRAFTBUKKIT_METADATA(CRAFTBUKKIT, "metadata"),
        CRAFTBUKKIT_POTION(CRAFTBUKKIT, "potion"),
        CRAFTBUKKIT_PROJECTILES(CRAFTBUKKIT, "projectiles"),
        CRAFTBUKKIT_SCHEDULER(CRAFTBUKKIT, "scheduler"),
        CRAFTBUKKIT_SCOREBOARD(CRAFTBUKKIT, "scoreboard"),
        CRAFTBUKKIT_UPDATER(CRAFTBUKKIT, "updater"),
        CRAFTBUKKIT_UTIL(CRAFTBUKKIT, "util");

        private final String path;

        PackageType(String path) {
            this.path = path;
        }

        PackageType(PackageType parent, String path) {
            this(parent + "." + path);
        }

        public String getPath() {
            return path;
        }

        public Class<?> getClass(String className) throws ClassNotFoundException {
            return Class.forName(this + "." + className);
        }

        @Override
        public String toString() {
            return path;
        }
    }

    public enum ServerVersion {
        MINECRAFT_1_12(3, "v1_12_R1"),
        MINECRAFT_1_11_1(2, "v1_11_R2"),
        MINECRAFT_1_11(1, "v1_11_R1"),
        UNKNOWN(0);

        Integer id;
        String version;

        ServerVersion(Integer id) {
            this.id = id;
        }

        ServerVersion(Integer id, String version) {
            this.id = id;
            this.version = version;
        }

        public Integer getId() {
            return id;
        }

        public String getVersion() {
            return version;
        }

        public static ServerVersion search() {
            for (ServerVersion version : values()) {
                if (getVersionName().equalsIgnoreCase(version.getVersion())) {
                    return version;
                }
            }
            return UNKNOWN;
        }

        public Boolean isNewer(ServerVersion version) {
            return id >= version.getId();
        }

        public Boolean isOlder(ServerVersion version) {
            return id < version.getId();
        }
    }
}