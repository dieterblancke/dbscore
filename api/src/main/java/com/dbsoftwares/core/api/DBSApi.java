package com.dbsoftwares.core.api;

/*
 * Created by DBSoftwares on 16/02/2018
 * Developer: Dieter Blancke
 * Project: DBSCore
 */

import com.dbsoftwares.core.api.language.ILanguageManager;
import com.dbsoftwares.core.api.user.UserCollection;

public interface DBSApi {

    ILanguageManager getLanguageManager();

    UserCollection getUsers();

}