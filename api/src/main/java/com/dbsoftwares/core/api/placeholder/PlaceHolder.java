package com.dbsoftwares.core.api.placeholder;

import com.dbsoftwares.core.api.user.User;
import com.dbsoftwares.core.api.utils.Utils;

public class PlaceHolder {

	String placeholder;
	PlaceHolderEventHandler eventHandler;

	public PlaceHolder(String placeholder, PlaceHolderEventHandler eventHandler) {
		this.placeholder = placeholder;
		this.eventHandler = eventHandler;
	}

	public String getPlaceHolder() {
		return placeholder;
	}

	public PlaceHolderEventHandler getEventHandler() {
		return eventHandler;
	}

	public String format(User user, String message) {
		if(placeholder == null || !message.contains(placeholder)) {
			return message;
		}
		PlaceHolderEvent event = new PlaceHolderEvent(user, this, message);
		String replace = eventHandler.getReplace(event);

		return message.replace(placeholder + (event.getArg() != null ? event.getArg() : ""), Utils.format(replace));
	}
}