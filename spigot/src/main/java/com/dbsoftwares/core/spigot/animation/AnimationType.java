package com.dbsoftwares.core.spigot.animation;

public enum AnimationType {
    TITLE, TAB, ACTIONBAR
}