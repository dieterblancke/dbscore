package com.dbsoftwares.core.spigot.animation.title;

import com.dbsoftwares.core.api.user.User;
import com.dbsoftwares.core.spigot.animation.AnimationLine;
import com.dbsoftwares.core.spigot.sendable.TitleObject;

public class TitleLine extends AnimationLine {

    private String title;
    private String subtitle;
    private Integer fadeIn = 0;
    private Integer stay = 20;
    private Integer fadeOut = 0;

    public TitleLine(String title, String subtitle) {
        this.title = title;
        this.subtitle = subtitle;
    }

    public TitleLine(String title, String subtitle, Integer fadeIn, Integer stay, Integer fadeOut) {
        this.title = title;
        this.subtitle = subtitle;
        this.fadeIn = fadeIn;
        this.stay = stay;
        this.fadeOut = fadeOut;
    }

    @Override
    public Integer getTicks() {
        return fadeIn + stay + fadeOut;
    }

    @Override
    public void send(User user) {
        new TitleObject(title == null ? "" : title, subtitle == null ? "" : subtitle, fadeIn, stay, fadeOut).send(user);
    }
}