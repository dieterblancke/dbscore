package com.dbsoftwares.core.spigot.sendable;

import com.dbsoftwares.core.api.DBSoftwares;
import com.dbsoftwares.core.api.user.User;
import com.dbsoftwares.core.api.utils.ReflectionUtils;
import com.dbsoftwares.core.api.utils.Utils;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Method;

public class TabObject implements Sendable {

    private static Class<?> packetTab;
    private static Class<?> nmsChatSerializer;
    private static Class<?> chatBaseComponent;
    @Getter @Setter private String header = "";
    @Getter @Setter private String footer = "";

    public TabObject() {
        loadClasses();
    }

    public TabObject(String header, String footer) {
        this.header = header;
        this.footer = footer;
        loadClasses();
    }

    private void loadClasses() {
        packetTab = ReflectionUtils.getNMS("PacketPlayOutPlayerListHeaderFooter");
        chatBaseComponent = ReflectionUtils.getNMS("IChatBaseComponent");
        nmsChatSerializer = ReflectionUtils.getNMS("ChatComponentText");
    }

    @Override
    public void send(User user) {
        try {
            Object empty = nmsChatSerializer.getConstructor(String.class).newInstance("");

            Object handle = ReflectionUtils.getHandle(user.getPlayer());
            Object connection = ReflectionUtils.getField(handle.getClass(), "playerConnection").get(handle);
            Method sendPacket = ReflectionUtils.getMethod(connection.getClass(), "sendPacket");

            Object packet = (ReflectionUtils.getVersion().isOlder(ReflectionUtils.ServerVersion.MINECRAFT_1_12) ?
                    packetTab.getConstructor(chatBaseComponent).newInstance(new Object[]{ null }) : packetTab.newInstance());

            Object serializedHeader = (header == null ? empty : nmsChatSerializer.getConstructor(String.class).newInstance(Utils.format(header)));
            Object serializedFooter = (footer == null ? empty : nmsChatSerializer.getConstructor(String.class).newInstance(Utils.format(footer)));

            ReflectionUtils.getField(packetTab, "a").set(packet, serializedHeader);
            ReflectionUtils.getField(packetTab, "b").set(packet, serializedFooter);

            sendPacket.invoke(connection, packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void broadcast(String permission) {
        DBSoftwares.getApi().getUsers().stream().filter(user -> user.hasPermission(permission)).forEach(this::send);
    }

    @Override
    public void broadcast() {
        DBSoftwares.getApi().getUsers().forEach(this::send);
    }
}