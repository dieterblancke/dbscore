package com.dbsoftwares.core.spigot.sendable;

import com.dbsoftwares.core.api.DBSoftwares;
import com.dbsoftwares.core.api.user.User;
import com.dbsoftwares.core.api.utils.ReflectionUtils;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.ChatColor;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

public class TitleObject implements Sendable {

    private static Class<?> packetTitle;
    private static Class<?> packetActions;
    private static Class<?> nmsChatSerializer;
    private static Class<?> chatBaseComponent;
    private static Field playerConnection;
    private static Method sendPacket;
    @Getter @Setter private String title = "";
    @Getter @Setter private String subtitle = "";
    @Getter @Setter private int fadeInTime = -1;
    @Getter @Setter private int stayTime = -1;
    @Getter @Setter private int fadeOutTime = -1;

    private static final Map<Class<?>, Class<?>> CORRESPONDING_TYPES = Maps.newHashMap();

    public TitleObject() {
        loadClasses();
    }

    public TitleObject(String title) {
        this.title = title;
        loadClasses();
    }

    public TitleObject(String title, String subtitle) {
        this.title = title;
        this.subtitle = subtitle;
        loadClasses();
    }

    public TitleObject(TitleObject title) {
        this.title = title.getTitle();
        this.subtitle = title.getSubtitle();
        this.fadeInTime = title.getFadeInTime();
        this.fadeOutTime = title.getFadeOutTime();
        this.stayTime = title.getStayTime();
        loadClasses();
    }

    public TitleObject(String title, String subtitle, int fadeInTime, int stayTime, int fadeOutTime) {
        this.title = title;
        this.subtitle = subtitle;
        this.fadeInTime = fadeInTime;
        this.stayTime = stayTime;
        this.fadeOutTime = fadeOutTime;
        loadClasses();
    }

    private void loadClasses() {
        if (packetTitle == null) {
            packetTitle = ReflectionUtils.getNMS("PacketPlayOutTitle");
            packetActions = ReflectionUtils.getNMS("PacketPlayOutTitle$EnumTitleAction");
            chatBaseComponent = ReflectionUtils.getNMS("IChatBaseComponent");
            nmsChatSerializer = ReflectionUtils.getNMS("ChatComponentText");
            playerConnection = ReflectionUtils.getField(ReflectionUtils.getNMS("EntityPlayer"), "playerConnection");
            sendPacket = ReflectionUtils.getMethod(ReflectionUtils.getNMS("PlayerConnection"), "sendPacket", ReflectionUtils.getNMS("Packet"));
        }
    }

    @Override
    public void send(User user) {
        try {
            Object handle = ReflectionUtils.getHandle(user.getPlayer());
            Object connection = playerConnection.get(handle);
            Object[] actions = packetActions.getEnumConstants();
            Object packet = packetTitle.getConstructor(packetActions, chatBaseComponent, Integer.TYPE, Integer.TYPE, Integer.TYPE).newInstance(actions[3], null, fadeInTime, stayTime, fadeOutTime);
            if (fadeInTime != -1 && fadeOutTime != -1 && stayTime != -1)
                sendPacket.invoke(connection, packet);

            Object serialized;
            if (!subtitle.equals("")) {
                serialized = nmsChatSerializer.getConstructor(String.class).newInstance(ChatColor.translateAlternateColorCodes('&', subtitle));
                packet = packetTitle.getConstructor(packetActions, chatBaseComponent).newInstance(actions[1], serialized);
                sendPacket.invoke(connection, packet);
            }

            serialized = nmsChatSerializer.getConstructor(String.class).newInstance(ChatColor.translateAlternateColorCodes('&', title));
            packet = packetTitle.getConstructor(packetActions, chatBaseComponent).newInstance(actions[0], serialized);
            sendPacket.invoke(connection, packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void broadcast() {
        DBSoftwares.getApi().getUsers().forEach(this::send);
    }

    @Override
    public void broadcast(String permission) {
        DBSoftwares.getApi().getUsers().stream().filter(user -> user.hasPermission(permission)).forEach(this::send);
    }
}