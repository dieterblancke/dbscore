package com.dbsoftwares.core.spigot.api;

/*
 * Created by DBSoftwares on 19/02/2018
 * Developer: Dieter Blancke
 * Project: DBSCore
 */

import com.dbsoftwares.core.api.DBSApi;
import com.dbsoftwares.core.api.language.ILanguageManager;
import com.dbsoftwares.core.api.user.UserCollection;
import com.dbsoftwares.core.api.user.UserList;
import com.dbsoftwares.core.spigot.server.ServerInfo;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.Optional;

public class DBSpigotAPI implements DBSApi {

    private UserList users;
    private List<ServerInfo> servers = Lists.newArrayList();

    public DBSpigotAPI() {
        users = new UserList();
    }

    @Override
    public ILanguageManager getLanguageManager() {
        return null;
    }

    @Override
    public UserCollection getUsers() {
        return users;
    }

    public List<ServerInfo> getServers() {
        return servers;
    }

    public List<String> getServerNames() {
        List<String> serverNames = Lists.newArrayList();
        servers.forEach(server -> serverNames.add(server.getName()));
        return serverNames;
    }

    public ServerInfo getOrCreateServer(String name) {
        if (name.contains(" ")) {
            return getServer(name);
        }
        if (servers.isEmpty()) {
            ServerInfo info = new ServerInfo(name, 0);
            servers.add(info);
            return info;
        }
        Optional<ServerInfo> optional = servers.stream().filter(server -> server.getName().equalsIgnoreCase(name)).findFirst();
        if (!optional.isPresent()) {
            ServerInfo info = new ServerInfo(name, 0);
            servers.add(info);
            return info;
        }
        return optional.get();
    }

    public ServerInfo getServer(String name) {
        return servers.stream().filter(server -> server.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }
}