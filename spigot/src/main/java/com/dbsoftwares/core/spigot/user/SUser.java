package com.dbsoftwares.core.spigot.user;

/*
 * Created by DBSoftwares on 21/02/2018
 * Developer: Dieter Blancke
 * Project: DBSCore
 */

import com.dbsoftwares.core.api.user.User;
import org.bukkit.entity.Player;

public class SUser implements User {

    private Player player;

    public SUser(Player player) {
        this.player = player;
    }

    @Override
    public boolean isPlayer() {
        return true;
    }

    @Override
    public boolean isProxiedPlayer() {
        return false;
    }

    @Override
    public Object getPlayer() {
        return player;
    }

    @Override
    public Object getProxiedPlayer() {
        return null;
    }

    @Override
    public String getName() {
        return player.getName();
    }

    @Override
    public boolean hasPermission(String permission) {
        return player.hasPermission(permission);
    }
}
