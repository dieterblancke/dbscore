package com.dbsoftwares.core.spigot.sendable;

import com.dbsoftwares.core.api.DBSoftwares;
import com.dbsoftwares.core.api.user.User;
import com.dbsoftwares.core.api.utils.ReflectionUtils;
import com.dbsoftwares.core.api.utils.Utils;
import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;

import java.lang.reflect.Method;

public class ActionBarObject implements Sendable {

    private static Class<?> packetActionBar;
    private static Class<?> nmsChatSerializer;
    private static Class<?> chatBaseComponent;
    @Getter @Setter private String bar = "";

    public ActionBarObject() {
        loadNMS();
    }

    public ActionBarObject(String bar) {
        this.bar = bar;
        loadNMS();
    }

    private void loadNMS() {
        packetActionBar = ReflectionUtils.getNMS("PacketPlayOutChat");
        chatBaseComponent = ReflectionUtils.getNMS("IChatBaseComponent");
        nmsChatSerializer = ReflectionUtils.getNMS("ChatComponentText");
    }

    @Override
    public void send(User user) {
        if(ReflectionUtils.getVersion().isOlder(ReflectionUtils.ServerVersion.MINECRAFT_1_12)) {
            try {
                Object handle = ReflectionUtils.getHandle(user.getPlayer());
                Object connection = ReflectionUtils.getField(handle.getClass(), "playerConnection").get(handle);
                Object networkManager = ReflectionUtils.getField(connection.getClass(), "networkManager").get(connection);
                Method sendPacket = ReflectionUtils.getMethod(connection.getClass(), "sendPacket");

                Object serialized = nmsChatSerializer.getConstructor(String.class).newInstance(Utils.format(bar));
                Object packet = packetActionBar.getConstructor(chatBaseComponent, byte.class).newInstance(serialized, (byte)2);
                sendPacket.invoke(connection, packet);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ((Player) user.getPlayer()).spigot().sendMessage(net.md_5.bungee.api.ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(Utils.format(bar)));
        }
    }

    @Override
    public void broadcast() {
        DBSoftwares.getApi().getUsers().forEach(this::send);
    }

    @Override
    public void broadcast(String permission) {
        DBSoftwares.getApi().getUsers().stream().filter(user -> user.hasPermission(permission)).forEach(this::send);
    }
}