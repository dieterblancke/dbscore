package com.dbsoftwares.core.spigot.animation.title;

import com.dbsoftwares.core.spigot.animation.Animation;
import com.dbsoftwares.core.spigot.animation.AnimationType;

public class TitleAnimation extends Animation {

    public TitleAnimation() {
        super(AnimationType.TITLE);
    }

    public void addTitle(String title) {
        lines.put(lines.size(), new TitleLine(title, null));
    }

    public void addTitle(String title, Integer fadeIn, Integer stay, Integer fadeOut) {
        add(title, null, fadeIn, stay, fadeOut);
    }

    public void addSubtitle(String subtitle) {
        lines.put(lines.size(), new TitleLine(null, subtitle));
    }

    public void addSubtitle(String subtitle, Integer fadeIn, Integer stay, Integer fadeOut) {
        add(null, subtitle, fadeIn, stay, fadeOut);
    }

    public void add(String title, String subtitle, Integer fadeIn, Integer stay, Integer fadeOut) {
        lines.put(lines.size(), new TitleLine(title, subtitle, fadeIn, stay, fadeOut));
    }

    @Override
    public void onEnd() {
        // do nothing
    }
}