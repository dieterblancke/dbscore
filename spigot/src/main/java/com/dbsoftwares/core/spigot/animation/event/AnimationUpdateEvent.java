package com.dbsoftwares.core.spigot.animation.event;

import com.dbsoftwares.core.spigot.animation.Animation;
import lombok.Data;

@Data
public abstract class AnimationUpdateEvent {

    public abstract void onUpdate(Animation animation);

}