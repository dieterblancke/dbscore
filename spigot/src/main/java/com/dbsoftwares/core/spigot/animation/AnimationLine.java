package com.dbsoftwares.core.spigot.animation;

import com.dbsoftwares.core.api.user.User;
import org.bukkit.entity.Player;

public abstract class AnimationLine {

    public abstract Integer getTicks();

    public abstract void send(User user);

    public void send(Iterable<User> users) {
        for(User user : users) {
            if(user != null && ((Player) user.getPlayer()).isOnline()) {
                send(user);
            }
        }
    }

    public void send(User[] users) {
        for(User user : users) {
            if(user != null && ((Player) user.getPlayer()).isOnline()) {
                send(user);
            }
        }
    }
}