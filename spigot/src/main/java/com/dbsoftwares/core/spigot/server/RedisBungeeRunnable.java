package com.dbsoftwares.core.spigot.server;

import com.dbsoftwares.core.api.DBSoftwares;
import com.dbsoftwares.core.spigot.DBSCore;
import com.dbsoftwares.core.spigot.api.DBSpigotAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;
import java.io.*;

public class RedisBungeeRunnable implements Runnable, PluginMessageListener {

    public RedisBungeeRunnable() {
        Bukkit.getMessenger().registerIncomingPluginChannel(DBSCore.getInstance(), "RedisBungee", this);
    }

    @Override
    public void run() {
        if (Bukkit.getOnlinePlayers().isEmpty()) {
            return;
        }
        Player p = Bukkit.getOnlinePlayers().stream().findAny().get();

        try {
            pingServer(p, "ALL");

            for (String server : ((DBSpigotAPI) DBSoftwares.getApi()).getServerNames()) {
                pingServer(p, server);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pingServer(Player p, String server) throws IOException {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);

        out.writeUTF("PlayerCount");
        out.writeUTF(server);

        p.sendPluginMessage(DBSCore.getInstance(), "RedisBungee", b.toByteArray());

        b.close();
        out.close();
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!channel.equals("RedisBungee")) {
            return;
        }
        ByteArrayInputStream bIn = new ByteArrayInputStream(message);
        DataInputStream in = new DataInputStream(bIn);
        try {
            String sub = in.readUTF();
            if (sub.equals("PlayerCount")) {
                String server = in.readUTF();
                if (server.equals("ALL")) {
                    int globalCount = in.readInt();

                    ServerInfo info = ((DBSpigotAPI) DBSoftwares.getApi()).getOrCreateServer(server);
                    info.setCount(globalCount);
                } else {
                    ServerInfo info = ((DBSpigotAPI) DBSoftwares.getApi()).getOrCreateServer(server);

                    if (info != null) {
                        info.setCount(in.readInt());
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bIn.close();
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}