package com.dbsoftwares.core.spigot.animation.tab;

import com.dbsoftwares.core.api.user.User;
import com.dbsoftwares.core.spigot.animation.AnimationLine;
import com.dbsoftwares.core.spigot.sendable.TabObject;

public class TabLine extends AnimationLine {

    private String header;
    private String footer;
    private Integer stay;

    public TabLine(String header, String footer, Integer stay) {
        this.header = header;
        this.footer = footer;
        this.stay = stay;
    }

    @Override
    public Integer getTicks() {
        return stay;
    }

    @Override
    public void send(User user) {
        new TabObject(header == null ? "" : header, footer == null ? "" : footer).send(user);
    }
}