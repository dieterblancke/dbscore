package com.dbsoftwares.core.spigot;

/*
 * Created by DBSoftwares on 19/02/2018
 * Developer: Dieter Blancke
 * Project: DBSCore
 */

import com.dbsoftwares.core.api.DBSoftwares;
import com.dbsoftwares.core.api.utils.ReflectionUtils;
import com.dbsoftwares.core.spigot.api.DBSpigotAPI;
import com.dbsoftwares.core.spigot.server.BungeeRunnable;
import com.dbsoftwares.core.spigot.server.RedisBungeeRunnable;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

public class DBSCore extends JavaPlugin {

    @Getter private static DBSCore instance;
    @Getter  private DBSpigotAPI api;

    @Override
    public void onEnable() {
        instance = this;
        saveConfig();

        this.api = new DBSpigotAPI();

        getServer().getScheduler().runTaskTimer(this, new BungeeRunnable(), 100, 100);
        getServer().getScheduler().runTaskTimer(this, new RedisBungeeRunnable(), 110, 100);

        initializeAPI(api);
    }

    private void initializeAPI(DBSpigotAPI api) {
        try {
            ReflectionUtils.getField(DBSoftwares.class, "api").set(null, api);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}