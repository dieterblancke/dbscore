package com.dbsoftwares.core.spigot.animation.actionbar;

import com.dbsoftwares.core.api.user.User;
import com.dbsoftwares.core.spigot.animation.AnimationLine;
import com.dbsoftwares.core.spigot.sendable.ActionBarObject;

public class ActionBarLine extends AnimationLine {

    private String bar;
    private Integer stay = 20;

    public ActionBarLine(String bar, Integer stay) {
        this.bar = bar;
        this.stay = stay;
    }

    public ActionBarLine(String bar) {
        this.bar = bar;
    }

    @Override
    public Integer getTicks() {
        return stay;
    }

    @Override
    public void send(User user) {
        new ActionBarObject(bar).send(user);
    }
}