package com.dbsoftwares.core.spigot.animation.actionbar;

import com.dbsoftwares.core.spigot.animation.Animation;
import com.dbsoftwares.core.spigot.animation.AnimationType;

public class ActionBarAnimation extends Animation {

    public ActionBarAnimation() {
        super(AnimationType.ACTIONBAR);
    }

    public void add(String bar) {
        lines.put(lines.size(), new ActionBarLine(bar));
    }

    public void add(String bar, Integer stay) {
        lines.put(lines.size(), new ActionBarLine(bar, stay));
    }

    @Override
    public void onEnd() {
        // do nothing
    }
}