package com.dbsoftwares.core.spigot.server;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor @Data
public class ServerInfo {

    private String name;
    private Integer count;

    /**
     * @return True if count > 0, false if not.
     */
    public Boolean guessOnline() {
        return count > 0;
    }
}