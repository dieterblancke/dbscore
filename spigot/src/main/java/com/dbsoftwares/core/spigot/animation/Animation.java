package com.dbsoftwares.core.spigot.animation;

import com.dbsoftwares.core.api.DBSoftwares;
import com.dbsoftwares.core.api.user.User;
import com.dbsoftwares.core.api.user.UserCollection;
import com.dbsoftwares.core.spigot.DBSCore;
import com.dbsoftwares.core.spigot.animation.event.AnimationFinishEvent;
import com.dbsoftwares.core.spigot.animation.event.AnimationStartEvent;
import com.dbsoftwares.core.spigot.animation.event.AnimationUpdateEvent;
import com.google.common.collect.Maps;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class Animation extends BukkitRunnable {

    @Getter
    public LinkedHashMap<Integer, AnimationLine> lines = Maps.newLinkedHashMap();
    @Getter
    AnimationType type;
    Boolean running = false;
    private Iterator<Map.Entry<Integer, AnimationLine>> iterator;
    private User user;
    private UserCollection users;
    private AnimationLine currentLine;
    private Integer ticks;
    private AnimationStartEvent animationStartEvent;
    private AnimationFinishEvent animationFinishEvent;
    private AnimationUpdateEvent animationUpdateEvent;

    public Animation(AnimationType type) {
        this.type = type;
    }

    public void onAnimationStart(AnimationStartEvent event) {
        this.animationStartEvent = event;
    }

    public void onAnimationFinish(AnimationFinishEvent event) {
        this.animationFinishEvent = event;
    }

    public void onAnimationUpdate(AnimationUpdateEvent event) {
        this.animationUpdateEvent = event;
    }

    public abstract void onEnd();

    public void playAnimation(User user) {
        if (animationStartEvent != null) {
            animationStartEvent.onStart(this);
        }
        this.user = user;
        if (running) {
            cancel();
        }
        runTaskTimer(DBSCore.getPlugin(DBSCore.class), 1, 1);
        running = true;
    }

    public void playAnimation(UserCollection users) {
        if (animationStartEvent != null) {
            animationStartEvent.onStart(this);
        }
        this.users = users;
        if (running) {
            cancel();
        }
        runTaskTimer(DBSCore.getPlugin(DBSCore.class), 1, 1);
        running = true;
    }

    public void broadcastAnimation() {
        if (animationStartEvent != null) {
            animationStartEvent.onStart(this);
        }
        if (running) {
            cancel();
        }
        this.users = DBSoftwares.getApi().getUsers();
        runTaskTimer(DBSCore.getPlugin(DBSCore.class), 1, 1);
        running = true;
    }

    public void stopAnimation() {
        users = null;
        iterator = null;
        cancel();
        onEnd();
    }

    public void run() {
        if (lines.isEmpty()) {
            return;
        }
        if (users != null) {
            users.removeIf(user -> !((Player) user.getPlayer()).isOnline());
        }
        if (user != null) {
            if (user.getPlayer() == null || !((Player) user.getPlayer()).isOnline()) {
                stopAnimation();
                return;
            }
        }
        if (iterator != null && !iterator.hasNext() && animationFinishEvent != null) {
            if (!animationFinishEvent.onFinish(this)) {
                stopAnimation();
                return;
            }
        }
        if (iterator == null || !iterator.hasNext()) {
            iterator = lines.entrySet().iterator();
        }
        if (currentLine == null || ticks == null || ticks > currentLine.getTicks()) {
            ticks = 0;
            currentLine = iterator.next().getValue();
            if (users == null) {
                currentLine.send(user);
            } else {
                currentLine.send(users);
            }
            if (animationUpdateEvent != null) {
                animationUpdateEvent.onUpdate(this);
            }
            return;
        }
        ticks += 1;
    }

    public void cancel() {
        super.cancel();
        running = false;
    }
}