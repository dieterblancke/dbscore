package com.dbsoftwares.core.spigot.sendable;

import com.dbsoftwares.core.api.user.User;

public interface Sendable {

    /**
     * Sends the object to a certain user. (Title, tab, actionbar, Chat Components, ...)
     * @param user The user you want to receive the message.
     */
    void send(User user);

    /**
     * Sends the object to all users with a certain rank. (Title, tab, actionbar, Chat Components, ...)
     * @param permission The users with the given permission you want to receive the message.
     */
    void broadcast(String permission);

    /**
     * Sends the object to all users. (Title, tab, actionbar, Chat Components, ...)
     */
    void broadcast();

}