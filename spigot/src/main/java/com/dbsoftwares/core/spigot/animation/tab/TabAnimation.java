package com.dbsoftwares.core.spigot.animation.tab;

import com.dbsoftwares.core.spigot.animation.Animation;
import com.dbsoftwares.core.spigot.animation.AnimationType;

public class TabAnimation extends Animation {

    public TabAnimation() {
        super(AnimationType.TAB);
    }

    public void addHeader(String header) {
        add(header, null, 20);
    }

    public void addFooter(String footer) {
        add(null, footer, 20);
    }

    public void add(String header, String footer, Integer stay) {
        lines.put(lines.size(), new TabLine(header, footer, stay));
    }

    @Override
    public void onEnd() {
        // do nothing
    }
}