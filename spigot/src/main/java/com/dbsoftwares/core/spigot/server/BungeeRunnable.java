package com.dbsoftwares.core.spigot.server;

/*
 * Created by DBSoftwares on 30 september 2017
 * Developer: Dieter Blancke
 * Project: centrixcore
 */

import com.dbsoftwares.core.api.DBSoftwares;
import com.dbsoftwares.core.spigot.DBSCore;
import com.dbsoftwares.core.spigot.api.DBSpigotAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.*;

public class BungeeRunnable implements Runnable, PluginMessageListener {

    public BungeeRunnable() {
        Bukkit.getMessenger().registerIncomingPluginChannel(DBSCore.getInstance(), "BungeeCord", this);
    }

    @Override
    public void run() {
        if (Bukkit.getOnlinePlayers().isEmpty()) {
            return;
        }
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            Player p = Bukkit.getOnlinePlayers().stream().findFirst().get();
            out.writeUTF("GetServers");

            if (b.toByteArray().length > 32760) {
                b.reset();
                return;
            }

            p.sendPluginMessage(DBSCore.getInstance(), "BungeeCord", b.toByteArray());

            if (!DBSCore.getInstance().getConfig().getBoolean("redis")) {
                try {
                    pingServer(p, "ALL");

                    for (String server : ((DBSpigotAPI) DBSoftwares.getApi()).getServerNames()) {
                        pingServer(p, server);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                b.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void pingServer(Player p, String server) throws IOException {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);

        out.writeUTF("PlayerCount");
        out.writeUTF(server);

        p.sendPluginMessage(DBSCore.getInstance(), "BungeeCord", b.toByteArray());

        b.close();
        out.close();
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!channel.equals("BungeeCord")) {
            return;
        }
        ByteArrayInputStream bIn = new ByteArrayInputStream(message);
        DataInputStream dIn = new DataInputStream(bIn);
        try {
            String sub = dIn.readUTF();

            if (sub.equals("GetServers")) {
                for (String server : dIn.readUTF().split(", ")) {
                    ((DBSpigotAPI) DBSoftwares.getApi()).getOrCreateServer(server);
                }
            } else if (sub.equals("PlayerCount")) {
                String server = dIn.readUTF();
                if (server.equals("ALL")) {
                    int globalCount = dIn.readInt();

                    ServerInfo info = ((DBSpigotAPI) DBSoftwares.getApi()).getOrCreateServer(server);
                    info.setCount(globalCount);
                } else {
                    ServerInfo info = ((DBSpigotAPI) DBSoftwares.getApi()).getOrCreateServer(server);

                    if (info != null) {
                        info.setCount(dIn.readInt());
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bIn.close();
                dIn.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}